<?php
/**
 * @xmlNamespace 
 * @xmlType TransportMeansType
 * @xmlName EnRouteEvent
 * @var EnRouteEventType
 */
class EnRouteEventType {



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\NameType $LocationName [optional] Place for en route event.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $CountryCode [optional] Country code for en route event.
		@param  $Incident [optional] En route event incident details.
		@param  $Control [optional] Control for transshipment or change in seals.
		@param  $TransShipment [optional] Details for transshipment en route.
		@param fi\tulli\schema\external\ncts\dme\v1\SealingType $Sealing [optional] Seals information.
	*/                                                                        
	public function __construct($LocationName = null, $CountryCode = null, $Incident = null, $Control = null, $TransShipment = null, $Sealing = null)
	{
		$this->LocationName = $LocationName;
		$this->CountryCode = $CountryCode;
		$this->Incident = $Incident;
		$this->Control = $Control;
		$this->TransShipment = $TransShipment;
		$this->Sealing = $Sealing;
	}
	
	/**
	 * @Definition Place for en route event.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName LocationName
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
	 */
	public $LocationName;
	/**
	 * @Definition Country code for en route event.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName CountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $CountryCode;
	/**
	 * @Definition En route event incident details.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Incident
	 */
	public $Incident;
	/**
	 * @Definition Control for transshipment or change in seals.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Control
	 */
	public $Control;
	/**
	 * @Definition Details for transshipment en route.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName TransShipment
	 */
	public $TransShipment;
	/**
	 * @Definition Seals information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Sealing
	 * @var fi\tulli\schema\external\ncts\dme\v1\SealingType
	 */
	public $Sealing;


} // end class EnRouteEventType
