<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName NotifyRequest
 * @var NotifyRequest
 * @xmlDefinition A notification from Customs to an intermediary. Used for signalling that one or more messages are available for download.
 */
class NotifyRequest
	{



	/**                                                                       
	*/                                                                        
	public function __construct($RequestHeader = null, $MessageInformation = null)
	{
		$this->RequestHeader = $RequestHeader;
		$this->MessageInformation = $MessageInformation;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName RequestHeader
	 * @var fi\tulli\ws\notificationservicetypes\v1\NotifyRequestHeader
	 */
	public $RequestHeader;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlMaxOccurs unbounded
	 * @xmlName MessageInformation
	 * @var fi\tulli\ws\corporateservicetypes\v1\MessageInformation
	 */
	public $MessageInformation;


} // end class NotifyRequest
