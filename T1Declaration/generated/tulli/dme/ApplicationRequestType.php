<?php

/**
 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
 * @xmlType 
 * @xmlName ApplicationRequest
 * @var ApplicationRequestType
 * @xmlDefinition Use for transmitting data. Can be XML Digital Signed
 */
class ApplicationRequestType
	{



	/**                                                                       
		@param fi\tulli\schema\corporateservice\v1\BusinessId $MessageBuilderBusinessId [optional] The business identity code of the party who built the ApplicationRequest document.
		@param  $MessageBuilderSoftwareInfo [optional] The name and version of the software which was used to build the ApplicationRequest document.
		@param fi\tulli\schema\corporateservice\v1\BusinessId $DeclarantBusinessId [optional] The business identity code of the party who is declarant.
		@param  $Timestamp [optional] Date and time when the ApplicationRequest document was built. Data type is ISODateTime. If no timezone is specified, the Finnish timezone is assumed.
		@param fi\tulli\schema\corporateservice\v1\Reference $Reference [optional] An identifier given by the customer (Message Builder) to identify this particular interchange. The reference must be unique for the combination of application and declarant.
		@param  $Environment [optional] Production or Test
		@param fi\tulli\schema\corporateservice\v1\ApplicationContent $ApplicationContent [optional] The actual declarant-owned message, e.g. customs declaration and information for it.
		@param org\w3\www\_2000\_09\xmldsig#\Signature $Signature [optional] Digital signature. This element is created by the XML Digital Signature operation by the message builder
	*/                                                                        
	public function __construct($MessageBuilderBusinessId = null, $MessageBuilderSoftwareInfo = null, $DeclarantBusinessId = null, $Timestamp = null, $Application = null, $Reference = null, $Environment = null, $ApplicationContent = null, $Signature = null)
	{
		$this->MessageBuilderBusinessId = $MessageBuilderBusinessId;
		$this->MessageBuilderSoftwareInfo = $MessageBuilderSoftwareInfo;
		$this->DeclarantBusinessId = $DeclarantBusinessId;
		$this->Timestamp = $Timestamp;
		$this->Application = $Application;
		$this->Reference = $Reference;
		$this->Environment = $Environment;
		$this->ApplicationContent = $ApplicationContent;
		$this->Signature = $Signature;
	}
	
	/**
	 * @Definition The business identity code of the party who built the ApplicationRequest document.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName MessageBuilderBusinessId
	 * @var fi\tulli\schema\corporateservice\v1\BusinessId
	 */
	public $MessageBuilderBusinessId;
	/**
	 * @Definition The name and version of the software which was used to build the ApplicationRequest document.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName MessageBuilderSoftwareInfo
	 */
	public $MessageBuilderSoftwareInfo;
	/**
	 * @Definition The business identity code of the party who is declarant.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName DeclarantBusinessId
	 * @var fi\tulli\schema\corporateservice\v1\BusinessId
	 */
	public $DeclarantBusinessId;
	/**
	 * @Definition Date and time when the ApplicationRequest document was built. Data type is ISODateTime. If no timezone is specified, the Finnish timezone is assumed.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName Timestamp
	 */
	public $Timestamp;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName Application
	 * @var fi\tulli\schema\corporateservice\v1\Application
	 */
	public $Application;
	/**
	 * @Definition An identifier given by the customer (Message Builder) to identify this particular interchange. The reference must be unique for the combination of application and declarant.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName Reference
	 * @var fi\tulli\schema\corporateservice\v1\Reference
	 */
	public $Reference;
	/**
	 * @Definition Production or Test
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName Environment
	 */
	public $Environment;
	/**
	 * @Definition The actual declarant-owned message, e.g. customs declaration and information for it.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName ApplicationContent
	 * @var fi\tulli\schema\corporateservice\v1\ApplicationContent
	 */
	public $ApplicationContent;
	/**
	 * @Definition Digital signature. This element is created by the XML Digital Signature operation by the message builder
	 * @xmlType element
	 * @xmlNamespace http://www.w3.org/2000/09/xmldsig#
	 * @xmlName Signature
	 * @var org\w3\www\_2000\_09\xmldsig#\Signature
	 */
	public $Signature;


} // end class ApplicationRequestType
