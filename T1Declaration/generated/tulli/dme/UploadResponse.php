<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName UploadResponse
 * @var UploadResponse
 * @xmlDefinition Response element for sending a application message operation.
 */
class UploadResponse
	{



	/**                                                                       
		@param fi\tulli\ws\corporateservicetypes\v1\MessageInformation $ApplicationRequestMessageInformation [optional] Not present when an error has occurred.
	*/                                                                        
	public function __construct($ResponseHeader = null, $ApplicationRequestMessageInformation = null)
	{
		$this->ResponseHeader = $ResponseHeader;
		$this->ApplicationRequestMessageInformation = $ApplicationRequestMessageInformation;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName ResponseHeader
	 * @var fi\tulli\ws\corporateservicetypes\v1\ResponseHeader
	 */
	public $ResponseHeader;
	/**
	 * @Definition Not present when an error has occurred.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName ApplicationRequestMessageInformation
	 * @var fi\tulli\ws\corporateservicetypes\v1\MessageInformation
	 */
	public $ApplicationRequestMessageInformation;


} // end class UploadResponse
