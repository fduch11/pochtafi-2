<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä4-MNO-900',

//    'TransitControlResultCode' => 'A3',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'RGY-889 PNO-15',
        'ConveyanceReferenceID' => null
    ),
//          'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'FI939990',

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 900,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 7500
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'Z',
        'LocationID' => 'FI534200'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("+1 days 10:00")), //+1 день, время 10:00 FI
        'LocationName' => 'FI'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => null, // -
    'ContainerTransportIndicator' => 'true',
    'Sealing' => null,
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'RU',
            'DestinationCountryCode' => 'FI',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Superior Household Ltd.',
                'Address' => array(
                    'Line' => 'Nevsky Prospect 108',
                    'PostcodeID' => '193036',
                    'CityName' => 'St. Petersburg',
                    'CountryCode' => 'RU'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Superior Household Ltd.',
                'Address' => array(
                    'Line' => 'Nevsky Prospect 108',
                    'PostcodeID' => '193036',
                    'CityName' => 'St. Petersburg',
                    'CountryCode' => 'RU'
                ),
            ),
            'Consignee' => array(
                'ID' => 'FI1234563-0',
                'IDExtension' => 'T0001',
                'Name' => 'Torangin Konepaja Oy',
                'Address' => array(
                    'Line' => 'Torangintaival 20',
                    'PostcodeID' => '93600',
                    'CityName' => 'Kuusamo',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => 'FI1234563-0',
                'IDExtension' => 'T0001',
                'Name' => 'Torangin Konepaja Oy',
                'Address' => array(
                    'Line' => 'Torangintaival 20',
                    'PostcodeID' => '93600',
                    'CityName' => 'Kuusamo',
                    'CountryCode' => 'FI'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => null,
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Sewing machines',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 7500
            ),
            'NetWeightMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 6800
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'ABCD123456789',
                    'PackageQuantity' => 900,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '00',
                    'DocumentID' => null
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => array(
                1 => array('TransportEquipmentID' => 'YMCY5549988'),
            ),
            'FreightPaymentMethodCode' => null

        )
    )
);

?>