<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä6-NMHJKL-bulkki',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'RGY-889 PNO-15',
        'ConveyanceReferenceID' => null
    ),
//          'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'HU101220',

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 13000
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("today 14:15")), // текущая 14:15 FI
        'LocationName' => 'FI'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+6 days")), // +6 дней от текущей
    'ContainerTransportIndicator' => 'true',
    'Sealing' => array(
        'SealQuantity' => 1,
        'SealID' => 'SI12345'
    ),
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'MD',
            'DestinationCountryCode' => 'SK',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Saunatonttu Oy',
                'Address' => array(
                    'Line' => 'Sihteerinpolku 56',
                    'PostcodeID' => '00670',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Saunatonttu Oy',
                'Address' => array(
                    'Line' => 'Sihteerinpolku 56',
                    'PostcodeID' => '00670',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Magyar saunas SA',
                'Address' => array(
                    'Line' => 'Vasut sor 20',
                    'PostcodeID' => '1038',
                    'CityName' => 'Budapest',
                    'CountryCode' => 'HU'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Magyar saunas SA',
                'Address' => array(
                    'Line' => 'Vasut sor 20',
                    'PostcodeID' => '1038',
                    'CityName' => 'Budapest',
                    'CountryCode' => 'HU'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => null,
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Palavaa kiveä',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 13000
            ),
            'NetWeightMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 12900
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'VR',
                    'PackagingMarksID' => 'Palavakivitehasse 030303',
                    'PackageQuantity' => null,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'VP1278978777'
                )
            ),


            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => array(
                1 => array('TransportEquipmentID' => 'NHKU 123456-7'),
            ),
            'FreightPaymentMethodCode' => null

        )
    )
);

?>