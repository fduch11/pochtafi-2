﻿<?php

$params = array(

    'TraderReferenceID' => 'Shipping 3',

    'TransitControlResultCode' => 'A3',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'Itella',
        'ConveyanceReferenceID' => null
    ),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'FI556100',

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => '1'
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI0109357-9R0002'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s'),
        'LocationName' => 'FI015300'
    ),

    'Unloading' => array(
        'LocationName' => 'Imatra'
    ),
	
    'Issue' => array(
        'IssueDate' => date('Y-m-d'),
        'LocationName' => 'Lappeenranta'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+3 day")),
    'ContainerTransportIndicator' => 'false',
    'Sealing' => null,
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'FI',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'DJPremium.com',
                'Address' => array(
                    'Line' => '7720 Kenamar Court Suite C',
                    'PostcodeID' => 'CA 92121',
                    'CityName' => 'San Diego',
                    'CountryCode' => 'US'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'DJPremium.com',
                'Address' => array(
                    'Line' => '7720 Kenamar Court Suite C',
                    'PostcodeID' => 'CA 92121',
                    'CityName' => 'San Diego',
                    'CountryCode' => 'US'
                ),
            ),
            'Consignee' => array(
                'ID' => 'FI2628792-7',
                'IDExtension' => 'T0001',
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Pelkolankatu 5',
                    'PostcodeID' => '53420',
                    'CityName' => 'Lappeenranta',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => 'FI2628792-7',
                'IDExtension' => 'T0001',
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Pelkolankatu 5',
                    'PostcodeID' => '53420',
                    'CityName' => 'Lappeenranta',
                    'CountryCode' => 'FI'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(640399),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Shoes',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '1'
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => '1703851',
                    'PackageQuantity' => 1,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 'T1',
                    'DocumentID' => 'CJ406371221US'
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        )
    )
);

?>