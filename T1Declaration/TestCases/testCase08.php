<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä8-12345678',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'B784598 AK8975',
        'ConveyanceReferenceID' => null
    ),
//          'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'FI499300',

    'GoodsItemQuantity' => '1',
    'TotalPackageQuantity' => 2080,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 2500
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("+1 days 08:00")), // +1 от текущей 8:00 FI
        'LocationName' => 'FI'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+7 days")), // +7 от текущей
    'ContainerTransportIndicator' => 'true',
    'Sealing' => null,
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'CZ',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Star Products Ltd.',
                'Address' => array(
                    'Line' => '15 Park Row',
                    'PostcodeID' => 'NY 10952',
                    'CityName' => 'New York',
                    'CountryCode' => 'US'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Star Products Ltd.',
                'Address' => array(
                    'Line' => '15 Park Row',
                    'PostcodeID' => 'NY 10952',
                    'CityName' => 'New York',
                    'CountryCode' => 'US'
                ),
            ),
            'Consignee' => array(
                'ID' => 'CZ125419875632147',
                'IDExtension' => null,
                'Name' => 'Lamper Lager Ag',
                'Address' => array(
                    'Line' => 'Rybna 8',
                    'PostcodeID' => '1',
                    'CityName' => 'Prague',
                    'CountryCode' => 'CZ'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => 'CZ125419875632147',
                'IDExtension' => null,
                'Name' => 'Lamper Lager Ag',
                'Address' => array(
                    'Line' => 'Rybna 8',
                    'PostcodeID' => '1',
                    'CityName' => 'Prague',
                    'CountryCode' => 'CZ'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => null,
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Golfmailoja',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 2500
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => '123-145XCE RT',
                    'PackageQuantity' => 2080,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '00',
                    'DocumentID' => null
                )
            ),

            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => array(
                1 => array('TransportEquipmentID' => 'HNKU12345-7'),
            ),
            'FreightPaymentMethodCode' => null

        )
    )
);

?>