<?php

$params = array(

    'XMessageType' => 'FITULR',

    'MovementReferenceID' => '14FI000000000529T6',
	
	'FunctionCode' => '4',
	'TransitTypeCode' => 'T1',
	
	'UnloadingRemarks' => array(
		'RemarksDate' => date('Y-m-d'),
		'LocationName' => 'Helsinki '
	),		
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI534200'
	),

	'GoodsItemQuantity' => '3',
	'TotalPackageQuantity' => '213',
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 19280
    ),
	
    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
    ),
	
	
	
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'FI',

            'Consignor' => array(
                'ID' => 'FI2234567-8',
                'IDExtension' => null,
                'Name' => 'Testikiito Oy',
                'Address' => array(
                    'Line' => 'Kouvolankuja 12',
                    'PostcodeID' => '56349',
                    'CityName' => 'Kouvola',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => 'FI3345678-9',
                'IDExtension' => null,
                'Name' => 'Oy Firma Ab',
                'Address' => array(
                    'Line' => 'Katu',
                    'PostcodeID' => '00000',
                    'CityName' => 'Kaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => '690700',
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Laattoja',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 30
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'JORO 1-10',
                    'PackageQuantity' => 3,
                    'PieceCountQuantity' => null
                ),
            ),
            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '380',
                    'DocumentID' => '17/03',
                    'SupplementaryInformation' => null
                ),
            ),
        ),

        2 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'FI',

            'Consignor' => array(
                'ID' => 'FI2234567-8',
                'IDExtension' => null,
                'Name' => 'Testikiito Oy',
                'Address' => array(
                    'Line' => 'Kouvolankuja 12',
                    'PostcodeID' => '56349',
                    'CityName' => 'Kouvola',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => 'FI3345678-9',
                'IDExtension' => null,
                'Name' => 'Oy Firma Ab',
                'Address' => array(
                    'Line' => 'Katu',
                    'PostcodeID' => '00000',
                    'CityName' => 'Kaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => '381600',
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Sementti',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 14250
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'SA',
                    'PackagingMarksID' => 'SEGO 1-160',
                    'PackageQuantity' => 160,
                    'PieceCountQuantity' => null
                )
            )
        ),

        3 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'RU',
            'DestinationCountryCode' => 'FI',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'ZAO Zavod',
                'Address' => array(
                    'Line' => 'Ul. Alexander Nevski 3',
                    'PostcodeID' => '00030',
                    'CityName' => 'St.Petersburg',
                    'CountryCode' => 'RU'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Kesport',
                'Address' => array(
                    'Line' => 'Kuunaritie 4',
                    'PostcodeID' => '23500',
                    'CityName' => 'Uusikaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => null,
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'jalkapalloja',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 5000
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'BX',
                    'PackagingMarksID' => 'Nike FB:1-1500',
                    'PackageQuantity' => 50,
                    'PieceCountQuantity' => null
                )
            )
        )

    )
	
	
		
);

?>