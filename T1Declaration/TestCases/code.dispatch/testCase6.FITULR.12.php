<?php

$params = array(

    'XMessageType' => 'FITULR',

    'MovementReferenceID' => '14FI000000000533T6',
	
	'FunctionCode' => '4',
	'TransitTypeCode' => 'T1',
	
	'UnloadingRemarks' => array(
		'RemarksDate' => date('Y-m-d'),
		'LocationName' => 'Helsinki'
	),		
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI534200'
	),

	'GoodsItemQuantity' => '2',
	'TotalPackageQuantity' => '2250',
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 9168
    ),

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
    ),
	
    'GoodsItem' => array(
        1 => array(
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'FI',

            'Consignor' => array(
                'ID' => 'FI2234567-8',
                'IDExtension' => null,
                'Name' => 'Testikiito Oy',
                'Address' => array(
                    'Line' => 'Kouvolankuja 12',
                    'PostcodeID' => '56349',
                    'CityName' => 'Kouvola',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => 'FI3345678-9',
                'IDExtension' => null,
                'Name' => 'Oy Firma Ab',
                'Address' => array(
                    'Line' => 'Katu',
                    'PostcodeID' => '00000',
                    'CityName' => 'Kaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '730',
                    'DocumentID' => 'PUUTTUU',
                    'SupplementaryInformation' => null
                ),
                2 => array(
                    'DocumentTypeCode' => '380',
                    'DocumentID' => 'PUUTTUU',
                    'SupplementaryInformation' => null
                ),
            ),

            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'JORO 1-10',
                    'PackageQuantity' => 10,
                    'PieceCountQuantity' => null
                ),
                2 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'JATE 1-5',
                    'PackageQuantity' => 2080,
                    'PieceCountQuantity' => null
                )
            ),

            'Commodity' => array(
                'TariffClassification' => '690700',
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Laattoja',
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 7000
            ),

        ),
        2 => array(
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'FI',

            'Consignor' => array(
                'ID' => 'FI2234567-8',
                'IDExtension' => null,
                'Name' => 'Testikiito Oy',
                'Address' => array(
                    'Line' => 'Kouvolankuja 12',
                    'PostcodeID' => '56349',
                    'CityName' => 'Kouvola',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => 'FI3345678-9',
                'IDExtension' => null,
                'Name' => 'Oy Firma Ab',
                'Address' => array(
                    'Line' => 'Katu',
                    'PostcodeID' => '00000',
                    'CityName' => 'Kaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'Sales 1-6',
                    'PackageQuantity' => 160,
                    'PieceCountQuantity' => null
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => '20091100',
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'mehua',
            ),
//            'SensitiveGoods' => array(
//                'SensitiveGoodsCode' => null,
//                'SensitiveGoodsMeasure' => 5.12
//            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 2168
            ),

        ),
    )
);

?>