﻿<?php

$params = array(

    'TraderReferenceID' => 'Shipping 2',

    'TransitControlResultCode' => 'A3',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'RU',
        'TransportMeansID' => 'A777AA 78',
        'ConveyanceReferenceID' => null
    ),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'FI542300',

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => '5.5'
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s'),
        'LocationName' => 'FI542300'
    ),

    'Unloading' => array(
        'LocationName' => 'St.-Petersburg'
    ),
	
    'Issue' => array(
        'IssueDate' => date('Y-m-d'),
        'LocationName' => 'Lappeenranta'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+1 day")),
    'ContainerTransportIndicator' => 'false',
    'Sealing' => null,
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'RU',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Quality Distributing LLC',
                'Address' => array(
                    'Line' => '1982 NE 25th Ave Ste 12',
                    'PostcodeID' => 'OR 97124',
                    'CityName' => 'Hillsboro',
                    'CountryCode' => 'US'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Quality Distributing LLC',
                'Address' => array(
                    'Line' => '1982 NE 25th Ave Ste 12',
                    'PostcodeID' => 'OR 97124',
                    'CityName' => 'Hillsboro',
                    'CountryCode' => 'US'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Rodion Shishkov',
                'Address' => array(
                    'Line' => 'Ligovsky pr. 5',
                    'PostcodeID' => '190000',
                    'CityName' => 'St.-Petersburg',
                    'CountryCode' => 'RU'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Rodion Shishkov',
                'Address' => array(
                    'Line' => 'Ligovsky pr. 5',
                    'PostcodeID' => '190000',
                    'CityName' => 'St.-Petersburg',
                    'CountryCode' => 'RU'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(850940),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Blender',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '5.5'
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'KSB1575MY 5-Speed',
                    'PackageQuantity' => 1,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 'VV',
                    'DocumentID' => '1234567'
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        )
    )
);

?>