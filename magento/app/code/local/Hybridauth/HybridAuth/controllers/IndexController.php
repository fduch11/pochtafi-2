<?php

class hybridauth_HybridAuth_IndexController extends Mage_Core_Controller_Front_Action{
	public function indexAction(){
	}
    public function authAction(){
		$helper = Mage::helper('hybridauth');
		$redirect = $helper->login($this->getRequest()->getParam('login'));
		
		if($redirect){
			if(strpos($redirect, 'http') > -1){
				$this->_redirectUrl($redirect);
			}else
				$this->_redirect($redirect);
		}
		else
			$this->_redirect('customer/account/create');
		
    }
	public function hauthAction(){
		$helper = Mage::helper('hybridauth');
		$helper->hauth();
	}
	public function emailAction(){
		$this->loadLayout();
		$block = $this->getLayout()->createBlock('hybridauth/email')->setTemplate('hybridauth/HybridAuth/hybridauth_email_form.phtml');
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
	}
	public function activationAction(){
		$helper = Mage::helper('hybridauth');
		$email = $this->getRequest()->getParam('email');
		if($email){
			$customer = Mage::getModel('customer/customer');
			$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
			$customer->loadByEmail($email);
			if ($customer->getId()) {
				$this->_redirect('auth/index/email', array('exists' => true));
			}else{
				$helper->emailActivation($this->getRequest()->getParam('email'));
				$redirect = 'customer/account/emailconfirmsent';
				$this->_redirect($redirect);
			}
		}else{
			$this->_redirect('auth/index/email', array('wrong' => true));
		}
	}
}