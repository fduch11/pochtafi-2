<?php
class hybridauth_HybridAuth_Block_Email extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected function _toHtml(){
		if(Mage::getSingleton('customer/session')->isLoggedIn() == 0){
			//Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())
			$current_url = Mage::getUrl('hybridauth/index/activation');
			$email = unserialize(Mage::getSingleton ('customer/session')->getUserProfile())->email;
			$email = $email? $email: '';
			$this->assign('current_url', $current_url);
			$this->assign('email', $email);
			$this->assign('exists', $this->getRequest()->getParam( 'exists' ));
			$this->assign('wrong', $this->getRequest()->getParam( 'wrong' ));
			return parent::_toHtml();
		}
		return '';
    }
} 