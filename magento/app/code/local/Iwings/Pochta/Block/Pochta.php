<?php
class Iwings_Pochta_Block_Pochta extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getPochta()     
     { 
        if (!$this->hasData('pochta')) {
            $this->setData('pochta', Mage::registry('pochta'));
        }
        return $this->getData('pochta');
        
    }
}