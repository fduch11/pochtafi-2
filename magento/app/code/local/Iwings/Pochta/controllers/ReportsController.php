<?php
require_once('BaseController.php');

class Iwings_Pochta_ReportsController extends Iwings_Pochta_BaseController
{
    public function indexAction()
    {
        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function filterPostAction()
    {
        $this->_redirect('pochta/reports/warehousePost', array('_query' => array('reception_type' => $_POST['reception_type'], 'begin' => $_POST['begin'], 'end' => $_POST['end'])));
    }

    public function warehousePostAction()
    {
        $begin = $this->getRequest()->getParam('begin');
        $end = $this->getRequest()->getParam('end');
        $reception_type = $this->getRequest()->getParam('reception_type');
        $origin = $this->selectOrigin($reception_type);

        if ($reception_type == Mage::registry('reception_type_lappeenranta')) {
            $store_id = ' FI2628792-7R0001';
        } elseif ($reception_type == Mage::registry('reception_type_imatra')) {
            $store_id = ' FI2628792-7R0003';
        }

        if ($reception_type == Mage::registry('reception_type_eu_lappeenranta')) {
            $store = Mage::registry('reception_type_lappeenranta');
        } elseif ($reception_type == Mage::registry('reception_type_eu_imatra')) {
            $store = Mage::registry('reception_type_imatra');
        } else {
            $store = $reception_type;
        }

        Mage::register('reception_type', $reception_type);
        Mage::register('store_id', $store_id);
        if (($begin != '') and ($end != '')) {
            Mage::register('collection_parcels', $this->makeReport($store, $origin, $begin, $end));
        }

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    protected function selectOrigin($reception_type)
    {
        switch ($reception_type) {
            case (Mage::registry('reception_type_portland')):
                return Mage::registry('box_origin_overseas');
                break;
            case (Mage::registry('reception_type_vantaa')):
                return Mage::registry('box_origin_overseas');
                break;
            case (Mage::registry('reception_type_lappeenranta')):
                return Mage::registry('box_origin_overseas');
                break;
            case (Mage::registry('reception_type_imatra')):
                return Mage::registry('box_origin_overseas');
                break;
            case (Mage::registry('reception_type_eu_lappeenranta')):
                return Mage::registry('box_origin_european');
                break;
            case (Mage::registry('reception_type_eu_imatra')):
                return Mage::registry('box_origin_european');
                break;
            default:
                return '';
        }
    }

    protected function makeReport($reception_type, $origin, $begin, $end)
    {
        $filter_begin = new DateTime($begin);
        $filter_end = new DateTime($end);
        $collection_parcels = array();

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', 'virtual')
            ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_movement'))
            ->addAttributeToFilter('reception_type', $reception_type);

        foreach ($collection as $in_movement) {
            $parcels_movement = Mage::helper('pochta')->getProductsByMovement($in_movement->getId());

            $in_date = new DateTime($in_movement->getMvtDstActualDate());
            $in_documents = $in_movement->getRelatedProductCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_document'));
            $in_doc = $in_documents->getFirstItem();
            $in_doc = $in_doc->getPrevDocumentTypecode() . ' ' . $in_doc->getPrevDocumentId();

            foreach ($parcels_movement as $parcel_link) {
                $parcel = Mage::getModel('catalog/product')->load($parcel_link->getProductId());
                $out_date = '';
                $out_doc = '';

                $out_movements = $parcel->getCrossSellProductCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_movement'));
                foreach ($out_movements as $movement) {
                    if ($in_movement->getMvtDstWhs() == $movement->getMvtSrcWhs()) {
                        $out_date = new DateTime($movement->getMvtSrcActualDate());
                        $out_documents = $movement->getRelatedProductCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_document'));
                        $out_doc = $out_documents->getFirstItem();
                        $out_doc = $out_doc->getPrevDocumentTypecode() . ' ' . $out_doc->getPrevDocumentId();
                    }
                }

                if (($reception_type != Mage::registry('reception_type_na')) and ($parcel->getBoxOrigin() == $origin)) {
                    if ($out_date == '') {
                        $collection_parcels[$parcel->getId()] = $this->saveToArray($parcel, $in_date, $out_date, $in_doc, $out_doc);
                    } elseif (($out_date != '') and ($filter_begin <= $out_date) and ($out_date <= $filter_end)) {
                        if ($out_date > $filter_end) {
                            $out_doc = '';
                            $out_date = '';
                        }
                        $collection_parcels[$parcel->getId()] = $this->saveToArray($parcel, $in_date, $out_date, $in_doc, $out_doc);
                    }
                } elseif ($reception_type == Mage::registry('reception_type_na')) {
                    if ($in_movement->getMvtDstStatus() == Mage::registry('mvt_dst_status_complete')) {
                        $out_doc = $in_doc;
                        $out_date = $in_date;
                    }
                    $collection_parcels[$parcel->getId()] = $this->saveToArray($parcel, $in_date, $out_date, $in_doc, $out_doc);
                }
            }
        }

        ksort($collection_parcels);
        return $collection_parcels;
    }

    protected function saveToArray($parcel, $in_date, $out_date, $in_doc, $out_doc)
    {
        return array(
            'parcel' => $parcel,
            'in_date' => $in_date,
            'out_date' => $out_date,
            'in_doc' => $in_doc,
            'out_doc' => $out_doc
        );
    }
}

?>