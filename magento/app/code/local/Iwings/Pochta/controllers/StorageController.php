<?php

require_once("BaseController.php");

class Iwings_Pochta_StorageController extends Iwings_Pochta_BaseController
{
    public function indexAction()
    {
		$this->loadLayout('pochta');
		$this->renderLayout();
    }

    public function saveParcelAction($product = null)
    {
        parent::saveParcelAction($product);

        $this->_redirect('pochta/storage/index');
    }

}