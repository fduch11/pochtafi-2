<?php

class Iwings_Actionaudit_Block_Actionaudit extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getActionaudit()
    {
        if (!$this->hasData('actionaudit')) {
            $this->setData('actionaudit', Mage::registry('actionaudit'));
        }
        return $this->getData('actionaudit');

    }
}