<?php

require "fixedParams.php";

$fixedParams['Principal'] = array(
    'ID' => $PrincipalID,
    'IDExtension' => 'T0003',
    'Name' => 'Pochta.fi Oy',
    'Address' => array(
        'Line' => 'Kultakuusenkuja 4',
        'PostcodeID' => '55610',
        'CityName' => 'Imatra',
        'CountryCode' => 'FI'
    ),
    'NatureQualifierCode' => 'R',
    'LanguageCode' => 'FI',
    'TIRHolderID' => null
);

$fixedParams['AuthorisedConsignee'] = array(
    'ID' => $BusinessId,
    'IDExtension' => 'T0003'
);

$fixedParams['TransitDepartureOffice'] = 'FI556100';

$fixedParams['GoodsLocation'] = array(
    'LocationQualifierCode' => 'L',
    'LocationID' => $BusinessId . 'R0003'
);

$fixedParams['LocationName'] = 'Imatra';

?>