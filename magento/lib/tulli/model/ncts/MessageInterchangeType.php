<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName MessageInterchangeType
 * @var MessageInterchangeType
 * @xmlDefinition Message interchange information.
 */
class MessageInterchangeType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\BusinessIDType $MessageSenderID [optional] Unique business identifier of the sender.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\BusinessIDType $MessageRecipientID [optional] Unique business identifier of the recipient.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\DateTimeType $MessageDateTime [optional] Date and time of preparation of this message.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\MessageReferenceIDType $MessageReferenceID [optional] Unique identifier of this message interchange.
	*/                                                                        
	public function __construct($MessageSenderID = null, $MessageRecipientID = null, $MessageDateTime = null, $MessageReferenceID = null)
	{
		$this->MessageSenderID = $MessageSenderID;
		$this->MessageRecipientID = $MessageRecipientID;
		$this->MessageDateTime = $MessageDateTime;
		$this->MessageReferenceID = $MessageReferenceID;
	}
	
	/**
	 * @Definition Unique business identifier of the sender.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MessageSenderID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\BusinessIDType
	 */
	public $MessageSenderID;
	/**
	 * @Definition Unique business identifier of the recipient.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MessageRecipientID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\BusinessIDType
	 */
	public $MessageRecipientID;
	/**
	 * @Definition Date and time of preparation of this message.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MessageDateTime
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\DateTimeType
	 */
	public $MessageDateTime;
	/**
	 * @Definition Unique identifier of this message interchange.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MessageReferenceID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\MessageReferenceIDType
	 */
	public $MessageReferenceID;


} // end class MessageInterchangeType
