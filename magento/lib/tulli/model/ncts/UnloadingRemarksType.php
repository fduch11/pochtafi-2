<?php

/* TODO: manual generated file */

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName UnloadingRemarks
 * @var UnloadingRemarksType
 * @xmlDefinition Unloading remarks information.
 */
class UnloadingRemarksType
{

	public function __construct($RemarksDate, $LocationName)
	{
		$this->RemarksDate = $RemarksDate;
        $this->LocationName = $LocationName;
	}
	
	/**
	 * @Definition The date of unloading remarks.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName RemarksDate
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\DateType
	 */
	public $RemarksDate;

    /**
     * @Definition The place of unloading remarks.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName LocationName
     * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
     */
    public $LocationName;

} // end class UnloadingRemarksType
