<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName PreviousDocumentType
 * @var PreviousDocumentType
 * @xmlDefinition Previous document.
 */
class PreviousDocumentType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\PreviousProcedureTypeCodeType $DocumentTypeCode [optional] Previous document type code, FI Customs code list 0012.
		@param fi\tulli\schema\external\ncts\dme\v1\PreviousDocumentIDType $DocumentID [optional] Identifier for previous document.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\ItemSequenceNumberType $ItemSequenceNumber [optional] Goods item sequence number.
	*/                                                                        
	public function __construct($DocumentTypeCode = null, $DocumentID = null, $ItemSequenceNumber = null)
	{
		$this->DocumentTypeCode = $DocumentTypeCode;
		$this->DocumentID = $DocumentID;
		$this->ItemSequenceNumber = $ItemSequenceNumber;
	}
	
	/**
	 * @Definition Previous document type code, FI Customs code list 0012.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName DocumentTypeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\PreviousProcedureTypeCodeType
	 */
	public $DocumentTypeCode;
	/**
	 * @Definition Identifier for previous document.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DocumentID
	 * @var fi\tulli\schema\external\ncts\dme\v1\PreviousDocumentIDType
	 */
	public $DocumentID;
	/**
	 * @Definition Goods item sequence number.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName ItemSequenceNumber
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\ItemSequenceNumberType
	 */
	public $ItemSequenceNumber;


} // end class PreviousDocumentType
