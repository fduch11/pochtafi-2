<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName SensitiveGoodsType
 * @var SensitiveGoodsType
 * @xmlDefinition Goods sensitivity information.
 */
class SensitiveGoodsType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\SensitiveGoodsCodeType $SensitiveGoodsCode [optional] Sensitivity code, FI Customs code list 0195
		@param fi\tulli\schema\external\ncts\dme\v1\SensitiveGoodsMeasureType $SensitiveGoodsMeasure [optional] Sensitive quantity.
	*/                                                                        
	public function __construct($SensitiveGoodsCode = null, $SensitiveGoodsMeasure = null)
	{
		$this->SensitiveGoodsCode = $SensitiveGoodsCode;
		$this->SensitiveGoodsMeasure = $SensitiveGoodsMeasure;
	}
	
	/**
	 * @Definition Sensitivity code, FI Customs code list 0195
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName SensitiveGoodsCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\SensitiveGoodsCodeType
	 */
	public $SensitiveGoodsCode;
	/**
	 * @Definition Sensitive quantity.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName SensitiveGoodsMeasure
	 * @var fi\tulli\schema\external\ncts\dme\v1\SensitiveGoodsMeasureType
	 */
	public $SensitiveGoodsMeasure;


} // end class SensitiveGoodsType
