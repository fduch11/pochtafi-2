<?php

/* TODO: manual generated file */

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName Control
 * @var ControlType
 * @xmlDefinition Control for transshipment or change in seals.
 */
class ControlType
{

	public function __construct($NotifiedIndicator)
	{
		$this->NotifiedIndicator = $NotifiedIndicator;
	}
	
	/**
	 * @Definition Idicator of transshipment and/or change in seals having already been notified in NCTS.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName NotifiedIndicator
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
	 */
	public $NotifiedIndicator;

} // end class ControlType
