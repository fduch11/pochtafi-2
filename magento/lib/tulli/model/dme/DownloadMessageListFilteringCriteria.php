<?php

/**
 * @xmlNamespace 
 * @xmlType http://tulli.fi/ws/corporateservicetypes/v1
 * @xmlName DownloadMessageListFilteringCriteria
 * @var DownloadMessageListFilteringCriteria
 * @xmlDefinition These elements are used to specify filtering criteria (for download list)
 */
class DownloadMessageListFilteringCriteria
	{



	/**                                                                       
		@param string $StartDate [optional] Date within or after which the messages were stored.
		@param string $EndDate [optional] Date within or before which the messages were stored.
		@param string $StartTimestamp [optional] Timestamp within or after which the messages were stored.
		@param string $EndTimestamp [optional] Timestamp within or before which the messages were stored.
		@param fi\tulli\schema\corporateservice\v1\Application $Application [optional] The short name of the originating Customs application.
	*/                                                                        
	public function __construct($StartDate = null, $EndDate = null, $StartTimestamp = null, $EndTimestamp = null, $MessageStatus = null, $Application = null)
	{
		$this->StartDate = $StartDate;
		$this->EndDate = $EndDate;
		$this->StartTimestamp = $StartTimestamp;
		$this->EndTimestamp = $EndTimestamp;
		$this->MessageStatus = $MessageStatus;
		$this->Application = $Application;
	}
	
	/**
	 * @Definition Date within or after which the messages were stored.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName StartDate
	 * @var string
	 */
	public $StartDate;
	/**
	 * @Definition Date within or before which the messages were stored.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName EndDate
	 * @var string
	 */
	public $EndDate;
	/**
	 * @Definition Timestamp within or after which the messages were stored.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName StartTimestamp
	 * @var string
	 */
	public $StartTimestamp;
	/**
	 * @Definition Timestamp within or before which the messages were stored.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName EndTimestamp
	 * @var string
	 */
	public $EndTimestamp;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName MessageStatus
	 * @var fi\tulli\ws\corporateservicetypes\v1\MessageStatus
	 */
	public $MessageStatus;
	/**
	 * @Definition The short name of the originating Customs application.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs unbounded
	 * @xmlName Application
	 * @var fi\tulli\schema\corporateservice\v1\Application
	 */
	public $Application;


} // end class DownloadMessageListFilteringCriteria
