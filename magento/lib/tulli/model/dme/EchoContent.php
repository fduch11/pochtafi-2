<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName EchoContent
 * @var EchoContent
 * @xmlDefinition Used for for checking purpose: input and output for echo function
 */
class EchoContent
	{



	/**                                                                       
		@param  $Text [optional] The text for echo function
		@param org\w3\www\_2000\_09\xmldsig#\Signature[] $Signature [optional] Digital signature. This element is created by the XML Digital Signature operation by the message builder
	*/                                                                        
	public function __construct($Text = null, $Signature = null)
	{
		$this->Text = $Text;
		$this->Signature = $Signature;
	}
	
	/**
	 * @Definition The text for echo function
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/v1
	 * @xmlMaxOccurs unbounded
	 * @xmlName Text
	 */
	public $Text;
	/**
	 * @Definition Digital signature. This element is created by the XML Digital Signature operation by the message builder
	 * @xmlType element
	 * @xmlNamespace http://www.w3.org/2000/09/xmldsig#
	 * @xmlMinOccurs 0
	 * @xmlName Signature
	 * @var org\w3\www\_2000\_09\xmldsig#\Signature[]
	 */
	public $Signature;


} // end class EchoContent
