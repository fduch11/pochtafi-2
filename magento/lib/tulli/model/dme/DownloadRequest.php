<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName DownloadRequest
 * @var DownloadRequest
 * @xmlDefinition Request element for download a application message operation.
 */
class DownloadRequest
	{



	/**                                                                       
	*/                                                                        
	public function __construct($RequestHeader = null, $DownloadMessageFilteringCriteria = null)
	{
		$this->RequestHeader = $RequestHeader;
		$this->DownloadMessageFilteringCriteria = $DownloadMessageFilteringCriteria;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName RequestHeader
	 * @var fi\tulli\ws\corporateservicetypes\v1\RequestHeader
	 */
	public $RequestHeader;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName DownloadMessageFilteringCriteria
	 * @var fi\tulli\ws\corporateservicetypes\v1\DownloadMessageFilteringCriteria
	 */
	public $DownloadMessageFilteringCriteria;


} // end class DownloadRequest
