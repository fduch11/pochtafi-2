<?xml version="1.0" encoding="UTF-8"?>
<!-- Direct Message Exchange component schema of qualified types -->
<xs:schema xmlns:qdt="http://tulli.fi/schema/external/common/dme/v1_0/qdt" xmlns:cdt="http://tulli.fi/schema/external/common/dme/v1_0/cdt" xmlns:udt="http://tulli.fi/schema/external/common/dme/v1_0/udt" xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://tulli.fi/schema/external/common/dme/v1_0/qdt" elementFormDefault="qualified" version="v1_0">
	<!-- Common schema components -->
	<xs:import namespace="http://tulli.fi/schema/external/common/dme/v1_0/udt" schemaLocation="Dme_UnqualifiedType.xsd"/>
	<xs:import namespace="http://tulli.fi/schema/external/common/dme/v1_0/cdt" schemaLocation="Dme_CodeListType.xsd"/>
	<!-- DUMMY GLOBAL ELEMENT, NOT USED FOR ANYTHING, BUT REQUIRED FOR TRANSFORMATION FUNCTION LIBRARY TOOL TO DISMISS VALIDATIN ERROR -->
	<xs:element name="DUMMY"/>
	<!-- TEXT DATA FORMATS - Descriptive text -->
	<xs:simpleType name="DescriptionType">
		<xs:annotation>
			<xs:documentation xml:lang="en">General form of descriptive text.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:TextType">
			<xs:minLength value="1"/>
			<xs:maxLength value="350"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="StatementDescriptionType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Additional information statement description text.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="qdt:DescriptionType">
			<xs:maxLength value="70"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- TEXT DATA FORMATS - Single line of text -->
	<xs:simpleType name="NameType">
		<xs:annotation>
			<xs:documentation xml:lang="en">General form of name text in a single line.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:LineType">
			<xs:minLength value="1"/>
			<xs:maxLength value="35"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="SupplementaryInformationType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Supplementary information text for an additional document, given in a single line.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:LineType">
			<xs:minLength value="1"/>
			<xs:maxLength value="26"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- TEXT DATA FORMATS - Contact information -->
	<xs:simpleType name="ContactNameType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Contact person name, given in a single line.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:LineType">
			<xs:minLength value="1"/>
			<xs:maxLength value="25"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="PhoneNumberType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Phone number.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:LineType">
			<xs:maxLength value="25"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="EmailAddressType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Email address.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:LineType">
			<xs:maxLength value="100"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- CODE FORMATS - Schema version -->
	<xs:simpleType name="SchemaVersionType">
		<xs:annotation>
			<xs:documentation xml:lang="en">This pattern restricts value format to be used in enumerations (don't modify this even if enumeration is specified).</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:CodeType">
			<xs:pattern value="v[1-9]+[0-9]*(_[0-9]*)?"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- IDENTIFIER FORMATS - Message, movement, transaction and document reference number -->
	<xs:simpleType name="MessageReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Message reference number, used to identify a message instance.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="6"/>
			<xs:maxLength value="14"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ConveyanceReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Conveyance reference number to identify a journey of a means of transport, for example voyage number, flight number, trip number.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="35"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="CustomsReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Transaction reference number, issued by FI Customs.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:length value="17"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="MovementReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Movement reference number (MRN), issued by Customs</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:length value="18"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="TraderReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Trader reference for a document.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="22"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="UniqueConsignmentReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Unique Consignment Reference (as definec by WCO and ISO 15459) is a unique identifier assigned to goods being subject to cross border transactions.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="70"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- IDENTIFIER FORMATS - Party identifiers -->
	<xs:simpleType name="BusinessIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Unique business party identifier.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="9"/>
			<xs:maxLength value="17"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="IDExtensionType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Extension of an identification number, issued by FI Customs</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:pattern value="[T|R]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- IDENTIFIER FORMATS - Identifiers for equipments and physical objects -->
	<xs:simpleType name="PackagingMarksIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Marks and numbers of packages or pieces</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:LineType">
			<xs:minLength value="1"/>
			<xs:maxLength value="42"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="SealIDType">
		<xs:annotation/>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="20"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- IDENTIFIER FORMATS - Commercial identifiers -->
	<xs:simpleType name="AccessKeyCodeIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Guarantee access key code.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:length value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="AccountNumberIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Bank account number identifier.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="35"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="GuaranteeReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Guarantee reference number.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="24"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="OtherGuaranteeReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Other guarantee reference number, e.g. TIR Carnet Number.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="35"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="PaymentReferenceIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Payment reference identifier.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="19"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="QuotaNumberIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Identification number of quota or import ceiling.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="1"/>
			<xs:maxLength value="6"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- IDENTIFIER FORMATS - Locations -->
	<xs:simpleType name="WarehouseIDType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Identification of a warehouse</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:IdentifierType">
			<xs:minLength value="2"/>
			<xs:maxLength value="17"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- IDENTIFIER FORMATS - Sequence and ordinal numbers -->
	<xs:simpleType name="ItemSequenceNumberType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Ordinal number indicating the position in a sequence, anf identifying the item within the sequence.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:SequenceNumberType">
			<xs:totalDigits value="3"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- NUMERIC FORMATS - Amount or monetary value -->
	<xs:complexType name="AmountType">
		<xs:annotation>
			<xs:documentation xml:lang="en">A number of monetary units specified in a currency where the unit of currency is explicit or implied</xs:documentation>
		</xs:annotation>
		<xs:simpleContent>
			<xs:restriction base="udt:AmountType">
				<xs:totalDigits value="15"/>
				<xs:fractionDigits value="2"/>
				<xs:attribute name="currencyCode" type="cdt:CurrencyCodeType" use="required">
					<xs:annotation>
						<xs:documentation xml:lang="en">Currency code, FI Customs code list 0145 or ISO 4217 standard</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:restriction>
		</xs:simpleContent>
	</xs:complexType>
	<!-- Percent ratio -->
	<xs:simpleType name="PercentType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Numeric information expressed as parts per hundred as determined by calculation.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:PercentType">
			<xs:totalDigits value="5"/>
			<xs:fractionDigits value="2"/>
			<xs:minInclusive value="0"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- Rate -->
	<xs:simpleType name="RateType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Numerical value of one thing corresponding proportionally to a certain value of some other thing.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:RateType">
			<xs:totalDigits value="5"/>
			<xs:fractionDigits value="2"/>
			<xs:minInclusive value="0"/>
		</xs:restriction>
	</xs:simpleType>
</xs:schema>
