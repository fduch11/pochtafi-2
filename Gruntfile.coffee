module.exports = (grunt) ->

  grunt.initConfig(

    stylus:
      options:
        compress: false
      main:
        expand: true
        cwd: "html/stylesheets/_src"
        src: "*.styl"
        dest: "html/stylesheets"
        ext: ".css"
      client_ui_new:
        expand: true
        cwd: "html/client-ui-new/_src/css"
        src: "*.styl"
        dest: "magento/skin/frontend/pochta/default/css"
        ext: ".css"
      kiosk_new:
        expand: true
        cwd: "html/kiosk-new/_src/css"
        src: "*.styl"
        dest: "html/kiosk-new/css"
        ext: ".css"

    autoprefixer:
      main:
        expand: true
        cwd: "html/stylesheets"
        src: "*.css"
        dest: "html/stylesheets"
        ext: ".css"
      client_ui_new:
        expand: true
        cwd: "magento/skin/frontend/pochta/default/css"
        src: "*.css"
        dest: "magento/skin/frontend/pochta/default/css"
        ext: ".css"
      kiosk_new:
        expand: true
        cwd: "html/kiosk-new/css"
        src: "*.css"
        dest: "html/kiosk-new/css"
        ext: ".css"

    concat:
      client_ui_new_js:
        options:
          # Simple js modules system https://gist.github.com/pozadi/8973106
          # (helps don't care about files order)
          banner: """
            window.ns=function(){function b(b,c){return c?a[b]=function(){
            var d={exports:{}};return c(d.exports,d),a[b]=function(){
            return d.exports},d.exports}:a[b]()}var a={};return b.initAll=
            function(){for(var b in a)a.hasOwnProperty(b)&&a[b]()},b}();
          """
          footer: ";ns.initAll();"
        files:
          'magento/skin/frontend/pochta/default/js/bundle.js': [
            'html/client-ui-new/_src/javascript/vendor/jquery-1.11.1.min.js'
            'html/client-ui-new/_src/javascript/vendor/kefir.min.js'
            'html/client-ui-new/_src/javascript/vendor/*.js'
            'html/client-ui-new/_src/javascript/modules/*.js'
          ]
      kiosk_new_js:
        options:
          # Simple js modules system https://gist.github.com/pozadi/8973106
          # (helps don't care about files order)
          banner: """
            window.ns=function(){function b(b,c){return c?a[b]=function(){
            var d={exports:{}};return c(d.exports,d),a[b]=function(){
            return d.exports},d.exports}:a[b]()}var a={};return b.initAll=
            function(){for(var b in a)a.hasOwnProperty(b)&&a[b]()},b}();
          """
          footer: ";ns.initAll();"
        files:
          'html/kiosk-new/javascript/bundle.js': [
            'html/kiosk-new/_src/javascript/vendor/jquery-1.11.1.min.js'
            'html/kiosk-new/_src/javascript/vendor/kefir.min.js'
            'html/kiosk-new/_src/javascript/vendor/*.js'
            'html/kiosk-new/_src/javascript/modules/*.js'
          ]

    # ejs:
    #   storage:
    #     options:
    #       trans: (str) -> str
    #       staticUrl: (part) -> "../#{part}"
    #       pageUrl: (name) -> "#{name}.html"
    #     cwd: "html/storage/_src/"
    #     src: "*.ejs"
    #     dest: "html/storage"
    #     expand: true
    #     ext: ".html"
    #   storage_php:
    #     options:
    #       trans: (str) -> "<?php echo $this->__('#{str}') ?>"
    #       staticUrl: (part) -> "<?php echo $this->getSkinUrl('#{part}') ?>"
    #       pageUrl: (name) -> "<?php echo $this->getUrl('#{name.replace(/-+/g, '/')}') ?>"
    #     cwd: "html/storage/_src/"
    #     src: "*.ejs"
    #     dest: "html/storage-php"
    #     expand: true
    #     ext: ".html"
    #   kiosk:
    #     cwd: "kiosk/_src/"
    #     src: "*.ejs"
    #     dest: "kiosk"
    #     expand: true
    #     ext: ".html"
    #   client_ui:
    #     cwd: "html/client-ui/_src/"
    #     src: "*.ejs"
    #     dest: "html/client-ui"
    #     expand: true
    #     ext: ".html"
    #   client_ui_new:
    #     options:
    #       trans: (str) -> str
    #       staticUrl: (part) -> part
    #       pageUrl: (name) -> "#{name}.html"
    #     cwd: "html/client-ui-new/_src/html/"
    #     src: "*.ejs"
    #     dest: "html/client-ui-new"
    #     expand: true
    #     ext: ".html"
    #   client_ui_new_php:
    #     options:
    #       trans: (str) -> "<?php echo $this->__('#{str}') ?>"
    #       staticUrl: (part) -> "<?php echo $this->getSkinUrl('#{part}') ?>"
    #       pageUrl: (name) -> "<?php echo $this->getUrl('#{name.replace(/-+/g, '/')}') ?>"
    #     cwd: "html/client-ui-new/_src/html/"
    #     src: "*.ejs"
    #     dest: "html/client-ui-new-php"
    #     expand: true
    #     ext: ".html"
    #   kiosk_new:
    #     options:
    #       trans: (str) -> str
    #       staticUrl: (part) -> part
    #       pageUrl: (name) -> "#{name}.html"
    #     cwd: "html/kiosk-new/_src/html/"
    #     src: "*.ejs"
    #     dest: "html/kiosk-new"
    #     expand: true
    #     ext: ".html"
    #   kiosk_new_php:
    #     options:
    #       trans: (str) -> "<?php echo $this->__('#{str}') ?>"
    #       staticUrl: (part) -> "<?php echo $this->getSkinUrl('#{part}') ?>"
    #       pageUrl: (name) -> "<?php echo $this->getUrl('#{name.replace(/-+/g, '/')}') ?>"
    #     cwd: "html/kiosk-new/_src/html/"
    #     src: "*.ejs"
    #     dest: "html/kiosk-new-php"
    #     expand: true
    #     ext: ".html"


    clean:
      css: {src: "html/stylesheets/*.css"}
      # html_storage: {src: "html/storage/*.html"}
      # html_kiosk: {src: "kiosk/*.html"}
      # html_client_ui: {src: "html/client-ui/*.html"}
      client_ui_new: {src: ["magento/skin/frontend/pochta/default/css", "magento/skin/frontend/pochta/default/js"]}
      kiosk_new: {src: ["html/kiosk-new/css", "html/kiosk-new/javascript"]}

    watch:
      # html_storage:
      #   files: ["html/storage/_src/**"]
      #   tasks: ["clean:html_storage", "ejs:storage"]
      # html_kiosk:
      #   files: ["kiosk/_src/**"]
      #   tasks: ["clean:html_kiosk", "ejs:kiosk"]
      # html_client_ui:
      #   files: ["html/client-ui/_src/**"]
      #   tasks: ["clean:html_client_ui", "ejs:client_ui"]
      css:
        files: ["html/stylesheets/_src/**"]
        tasks: ["clean:css", "stylus", "autoprefixer"]
      client_ui_new:
        files: ["html/client-ui-new/_src/**"]
        tasks: ["client_ui_new"]
      kiosk_new:
        files: ["html/kiosk-new/_src/**"]
        tasks: ["kiosk_new"]

  )

  require("load-grunt-tasks")(grunt)

  grunt.registerTask "default", [
    "clean",
    # "ejs",
    "stylus", "autoprefixer",
    "concat:client_ui_new_js",
    "concat:kiosk_new_js"
  ]

  grunt.registerTask "client_ui_new", [
    "clean:client_ui_new"
    # "ejs:client_ui_new",
    # "ejs:client_ui_new_php"
    "stylus:client_ui_new", "autoprefixer:client_ui_new", "concat:client_ui_new_js"
  ]

  grunt.registerTask "kiosk_new", [
    "clean:kiosk_new"
    # "ejs:kiosk_new", "ejs:kiosk_new_php"
    "stylus:kiosk_new", "autoprefixer:kiosk_new", "concat:kiosk_new_js"
  ]
