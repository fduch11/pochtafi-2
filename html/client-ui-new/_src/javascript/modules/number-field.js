ns('number-field', function(exports) {

  function isNumberOrSpecial(keyEvent, options) {

    options = _.extend({allowFloat: false}, options);

    var code = keyEvent.keyCode;

    // backspace, delete, tab, escape, enter
    var allowed = ([46, 8, 9, 27, 13, 110].indexOf(code) !== -1);

    // . and ,
    allowed = allowed || (options.allowFloat && (code === 190 || code === 188));

    // Ctrl+A
    // allowed = allowed || (code == 65 && keyEvent.ctrlKey === true);

    // home, end, left, right
    allowed = allowed || (code >= 35 && code <= 39);

    // number but without Shift
    allowed = allowed || (!keyEvent.shiftKey && code >= 48 && code <= 57);

    // not sure (http://stackoverflow.com/a/469362/478603)
    allowed = allowed || (code >= 96 && code <= 105);

    // allow all Ctrl, Alt, Cmd, Shift (will cleanupt in after check)
    allowed = allowed || keyEvent.ctrlKey || keyEvent.altKey || keyEvent.metaKey || keyEvent.shiftKey;

    return allowed;
  }

  function removeNonDigits(text, options) {
    options = _.extend({allowFloat: false}, options);
    var regex = options.allowFloat ? (/[^0-9\.,]/g) : (/[^0-9]/g);
    text = text.replace(regex, '');
    text = text.replace(/[\.,]+/g, '.');
    if (text.replace(/[^\.]/g, '').length > 1) {
      text = parseFloat(text).toString();
    }
    if (options.floatFixed) {
      var parts = text.split('.');
      if (parts.length === 2) {
        parts[1] = parts[1].slice(0, parseInt(options.floatFixed, 10));
        text = parts.join('.');
      }
    }
    return text;
  }

  var initd = 'number-field-initialised';

  exports.init = function() {
    $('.js-number-field').each(function() {

      var $el = $(this);

      var options = {
        allowFloat: !!$el.data('allow-float'),
        floatFixed: $el.data('float-fixed'),
        positiveOnly: !!$el.data('positive-only'),
      };

      if (!$el.data(initd) && $el.parents('.js-template').length === 0) {

        $el.data(initd, true);

        // pre check
        $el.keydown(function (e) {
          if (!isNumberOrSpecial(e, options)) {
            e.preventDefault();
          }
        });

        // after check
        $el.on('input keyup paste change', function() {
          var value = removeNonDigits($el.val(), options);
          if (options.positiveOnly && value === '0') {
            value = '';
          }
          if (value != $el.val()) {
            $el.val(value);
          }
        });

      }

    });
  }


});
