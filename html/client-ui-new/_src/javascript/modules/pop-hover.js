ns('pop-hover', function() {

  $('.js-pop-hover').each(function() {

    var $source = $(this);
    var $target = $source.parents('.js-pop-hover__target');

    $source.hover(
      function() {$target.addClass('is-hovered')},
      function() {$target.removeClass('is-hovered')}
    );

  });

});
