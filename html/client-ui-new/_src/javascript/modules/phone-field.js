ns('phone-field', function(exports) {



  exports.init = function() {

    var initd = 'phone-field-initialised';

    $('.js-phone-field').each(function() {

      var $wrap = $(this);

      if (!$wrap.data(initd) && $wrap.parents('.js-template').length === 0) {
        $wrap.data(initd, true);

        $wrap.find('input').autotab('number');

      }

    });

  };


});
