ns('popover', function(exports) {

  $(document).click(function(e) {
    if ($(e.target).closest('.js-popover-target').length === 0) {
      $('.js-popover').removeClass('is-visible');
    }
  });

  exports.init = function() {

    $('.js-popover-target').each(function() {
      var $target = $(this);
      var $popover = $('.js-popover', this);
      $target.on('click touchend', function() {
        if (!$popover.hasClass('is-visible')) {
          $('.js-popover').removeClass('is-visible');
          $popover.addClass('is-visible');
        } else {
          $popover.removeClass('is-visible');
        }
      });
    });

  };


});



